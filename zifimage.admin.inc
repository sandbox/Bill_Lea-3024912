<?php

/**
 * @file
 * Provides administration pages for the zifimage module
 */

/**
 * Form callback for zifimage admin settings.
 */
function zifimage_admin_settings($form, &$form_state)
{
    // Aspect ratio options.
    $aspect_list = array(
    1 => 'Aspect Ratio',
    2 => 'Aspect Ratio From Teaser Image Aspect Ratio',
    3 => 'No Aspect Ratio Height, Width specified',
    );

    // ======= viewer select precalcs.
    $viewer_list = get_viewer_list();

    // ======= skin select precalcs.
    $zifimage_skins_list = array();
    $library_path = libraries_get_path('zoomify', false) . '/Assets/Skins';
    $zifimage_skins_list = scandir($library_path);
    array_shift($zifimage_skins_list);
    array_shift($zifimage_skins_list);

    $form['zifimage_viewer_skins'] = array(
    '#type'        => 'select',
    '#title'       => t('Availiable viewer skins'),
    '#options'     => $zifimage_skins_list,
    '#default_value' => variable_get('zifimage_viewer_skins', null),
    );

    $form['zifimage_conf_selected_viewer'] = array(
    '#type'         => 'select',
    '#title'        => t('Zoomify avaliable viewers'),
    '#options'      => $viewer_list,
    '#default_value' => variable_get('zifimage_selected_viewer', 1),
    '#empty_value' => 0,
    );

    $form['zifimage_conf_background_color'] = array(
    '#type' => 'textfield',
    '#title' => t('Zoomify Viewer Background color'),
    '#default_value' => variable_get('zifimage_conf_background_color', '#ffffff'),
    '#description' => t('Color of the Viewere background'),
    '#size' => 12,
    );

    $form['zifimage_security'] = array(
    '#type' => 'radios',
    '#title' => t('ZIF Image Security Level'),
    '#options' => array(
      ZIFIMAGE_SECURITY_LEVEL_NONE           => t('No Validation'),
      ZIFIMAGE_SECURITY_LEVEL_CLEAN_STRINGS  => t('String cleaning Only'),
      ZIFIMAGE_SECURITY_LEVEL_CLEAN_COMMANDS => t('Max Validation- Command and value validation'),
    ),
    '#default_value' => variable_get('zifimage_security', ZIFIMAGE_SECURITY_LEVEL_NONE),
    );

    $form['zifimage_conf_max_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Zoomify Frame maximum width'),
    '#default_value' => variable_get('zifimage_conf_max_width', 800),
    '#description' => t('Maximum Width of the zifimage frame.'),
    '#field_suffix' => t('px/%'),
    '#size' => 12,
    );

    $form['zifimage_conf_asp_op_mode'] = array(
    '#type'         => 'select',
    '#title'        => t('Zoomify Aspect Ratio Mode'),
    '#options'      => $aspect_list,
    '#default_value' => variable_get('zifimage_conf_asp_op_mode', 1),
    );

    $form['zifimage_conf_asp_str'] = array(
    '#type'         => 'textfield',
    '#title'        => t('Zoomify Aspect Ratio'),
    '#default_value' => variable_get('zifimage_conf_asp_str', '16:9'),
    '#size' => 12,
    '#states' => array(
      'visible' => array(
        ':input[name="zifimage_conf_asp_op_mode"]' => array('value' => (string) 1),
      ),
    ),
    );

    $form['zifimage_conf_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame width'),
    '#default_value' => variable_get('zifimage_conf_width', 800),
    '#description' => t('Width of the zifimage frame.'),
    '#field_suffix' => t('px'),
    '#size' => 12,
    '#states' => array(
      'visible' => array(
        ':input[name="zifimage_conf_asp_op_mode"]' => array('value' => (string) 3),
      ),
    ),
    );

    $form['zifimage_conf_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Frame height'),
    '#default_value' => variable_get('zifimage_conf_height', 600),
    '#description' => t('Height of the zifimage frame.'),
    '#field_suffix' => t('px'),
    '#size' => 12,
    '#states' => array(
      'visible' => array(
        ':input[name="zifimage_conf_asp_op_mode"]' => array('value' => (string) 3),
      ),
    ),
    );

    $form['zifimage_conf_html_cmds'] = array(
    '#title' => t('Zoomify command options'),
    '#type' => 'textarea',
    '#description' => t(
        'Additional options passed to the zif image viewer , separated by ampersands (&). For example: <code>zNavigatorVisible=false</code>.
                         The full list of variables is shipped with zoomify documentation but you can <a href="@faq">find it here</a>.',
        array('@faq' => 'http://www.zoomify.com/support.htm#a20081222_2050')
    ),
    '#default_value' => variable_get('zifimage_conf_html_cmds', null),

    );

    return system_settings_form($form);
}

function zifimage_admin_settings_validate($form_id, $form_values)
{
    // This function works.
    $test = $form_values['values']['zifimage_conf_html_cmds'];

    $security_level = $form_values['values']['zifimage_security'];
    $result = validate_user_commands($test, $security_level);
    if ($result) {
    } else {
        // Form commands are invalid form needs to be revised.
        $message = 'Validation failed revisew commnds';
        $name = 'zifimage_conf_html_cmds';
        form_set_error($name, $message, null);
    }
}
