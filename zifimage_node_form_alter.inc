<?php

/**
 * @file
 * This file contains form validation routines.
 */

/**
 * This function implements hook_form_BASE_FORM_ID_alter.
 * The purpose of this form alteration is to make the ibput
 * form simpler for the cases where you have no need to input an XML or other
 * support files or override the system configuration.
 */
function zifimage_form_zifimage_node_form_alter(&$form, &$form_state, $form_id)
{
    /*  $form['zifimage_asp_str'] = array(
    '#type' => 'textfield',
    '#title' => t('Zoomify Aspect Ratio'),
    '#weight' => 36,
    '#size' => 10,
    '#default_value' => variable_get('zifimage_consp_str', '16:9')f_a,
    '#states' => array(
      'visible' => array(
        ':input[name="zifimage_asp_op_mode"]' => array(
          'value' => (string) 1,
        ),
      ),
    ),
    );*/

    $viewer_list = get_viewer_list();

    $form['zifimage_max_width']['#weight'] = 33;

    $form['zifimage_asp_str']['#states'] = array(
    'visible' => array(
      ':input[name="zifimage_asp_op_mode[und]"]' => array(
        'value' => (string) '1',
      ),
    ),
    );

    //  $form['zifimage_asp_str']['#default_value'] = "16:9";
    $form['zifimage_asp_str']['#weight'] = "40";
    $form['zifimage_height']['#states'] = array(
    'visible' => array(':input[name="zifimage_asp_op_mode[und]"]' => array('value' => (string) '3')),
    );

    $form['zifimage_width']['#states'] = array(
    'visible' => array(':input[name="zifimage_asp_op_mode[und]"]' => array('value' => (string) '3')),
    );

    $form['fs1'] = array(
    '#type' => 'fieldset',
    '#title' => t('Zoomify Grouped Files'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#weight' => 50,
    );

    $form['fs2'] = array(
    '#type' => 'fieldset',
    '#title' => t('Misc Zoomify Optional Commands'),
    '#collapsible' => true,
    '#collapsed' => true
    , '#weight' => 51,
    );

    $form['fs1']['zifimage_bulk_txt_xml'] = $form['zifimage_bulk_txt_xml'];
    unset($form['zifimage_bulk_txt_xml']);

    $form['fs1']['zifimage_bulk_images'] = $form['zifimage_bulk_images'];
    unset($form['zifimage_bulk_images']);

    $form['fs1']['zifimage_hotspot_icons'] = $form['zifimage_hotspot_icons'];
    unset($form['zifimage_hotspot_icons']);

    $form['fs1']['zifimage_audio_media'] = $form['zifimage_audio_media'];
    unset($form['zifimage_audio_media']);

    $form['fs2']['zifimage_local_var'] = $form['zifimage_local_var'];
    unset($form['zifimage_local_var']);

    $form['zifimage_viewer'] = array(
    '#type' => 'select',
    '#title' => t('Zoomify avaliable viewers'),
    '#weight' => 34,
    '#options' => $viewer_list,
    '#default_value' => variable_get('zifimage_conf_selected_viewer', null),
    '#empty_value' => 0,
    );


    return $form;
}
