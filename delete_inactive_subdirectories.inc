</php
/** The php file deletion process does not remove all the directories when the 
 * node is deleted. We need to periodically clean up the zifimzge directory so
 * that unused nodes are removed. This function should be run eiter by chron or
 * whenever a node is deleted. 
 * The first Item is to build a list of active nodes and a list of directory 
 * paths. Comparing the two If a node directory path is not found in the active
 * list then delet it.
 **/
 function delete_inactive_subdirectories(){
   //build the active node list
   $nid_list=array();
   $node_type='zifimage';
   $nid_lost = db_query("SELECT nid FROM {node} WHERE type = :type", array(':type' => $node_type))
   ->fetchCol();
   //build a list of directories keyed yo their old node number
   $pattern='./files/zifimage/Images/*';
   $dir_list=glob($pattern,GLOB_ONLYDIR);
   foreach($dir_list as $dir_path){
     //get node number
     $pos=strrpos($dir_path,'/',0);
     $len=strlen($dir_path);
     if($pos !==false){
       $node_number=substr($dir_path,-($pos+1));
       $dir_path=substr($dir_path,0,$loc-1);
       $node_dir_list=($node_number => ('public://' . $dir_path));
     }//end if
   }//end foreach
   // List are built compaire them using  get_value(&$my_key,&$my_array)
   get_value(&$my_key,&$my_array)
   foreach($ir_list $nid=>$path){
     if(get_value($nid,$nid_list)=== false){
       file_unmanaged_delete( $node_dir[$nid]);
     }endif
   }//end foreach   
   $pattern='./files/zifimage/Assets/Hotspots/*';
   $dir_list=glob($pattern,GLOB_ONLYDIR);
   foreach($dir_list as $dir_path){
     //get node number
     $pos=strrpos($dir_path,'/',0);
     $len=strlen($dir_path);
     if($pos !==false){
       $node_number=substr($dir_path,-($pos+1));
       $dir_path=substr($dir_path,0,$loc-1);
       $node_dir_list=($node_number => ('public://' . $dir_path));
     }//end if
   }//end foreach
   // List are built compaire them using  get_value(&$my_key,&$my_array)
   get_value(&$my_key,&$my_array)
   foreach($ir_list $nid=>$path){
     if(get_value($nid,$nid_list)=== false){
       file_unmanaged_delete( $node_dir[$nid]);
     }endif
   }//end foreach  
   $pattern='./files/zifimage/Assets/txt_xml_files/*';
   $dir_list=glob($pattern,GLOB_ONLYDIR);
   foreach($dir_list as $dir_path){
     //get node number
     $pos=strrpos($dir_path,'/',0);
     $len=strlen($dir_path);
     if($pos !==false){
       $node_number=substr($dir_path,-($pos+1));
       $dir_path=substr($dir_path,0,$loc-1);
       $node_dir_list=($node_number => ('public://' . $dir_path));
     }//end if
   }//end foreach
   // List are built compaire them using  get_value(&$my_key,&$my_array)
   get_value(&$my_key,&$my_array)
   foreach($ir_list $nid=>$path){
     if(get_value($nid,$nid_list)=== false){
       file_unmanaged_delete( $node_dir[$nid]);
     }endif
   }//end foreach 
   $pattern='./files/zifimage/Assets/Media/*';
   $dir_list=glob($pattern,GLOB_ONLYDIR);
   foreach($dir_list as $dir_path){
     //get node number
     $pos=strrpos($dir_path,'/',0);
     $len=strlen($dir_path);
     if($pos !==false){
       $node_number=substr($dir_path,-($pos+1));
       $dir_path=substr($dir_path,0,$loc-1);
       $node_dir_list=($node_number => ('public://' . $dir_path));
     }//end if
   }//end foreach
   // List are built compaire them using  get_value(&$my_key,&$my_array)
   get_value(&$my_key,&$my_array)
   foreach($ir_list $nid=>$path){
     if(get_value($nid,$nid_list)=== false){
       file_unmanaged_delete( $node_dir[$nid]);
     }endif
   }//end foreach      
 }//end function
