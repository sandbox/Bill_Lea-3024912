<?php

/**
 * @file
 * This file provides all the functions needed in value validation.
 *
 * Two form validations must be performed. one when configuration data is
 * submitted and again when the node is ceated.
 */

/*
 * Function cleans string.
 */
function cleaner($dirty_str, $security_level)
{
    if (empty($dirty_str)) {
        return "";
    }
    switch ($security_level) {
        case ZIFIMAGE_SECURITY_LEVEL_NONE:
            return $dirty_str;

        break;

        case ZIFIMAGE_SECURITY_LEVEL_CLEAN_STRINGS:
            return clean_string($dirty_str);

        break;

        case ZIFIAMGE_SECURITY_LEVEL_CLEAN_COMMANDS:
            $clean_str = clean_string($dirty_str);
            if (validate_commands($clean_str) === true) {
                return $clean_str;
            }
            return "";

        break;
    }//end switch
}

/**
 * FUNCTION CLEANS STRINGS OF html cOMMANDS.
 */

function clean_string($dirty_str)
{
    if (empty($dirty_str)) {
        return "";
    }
    // If there is no zcommand return the empty string.
    $pos = strpos($dirty_str, "z");
    /*
    * all valid command string should start with a z no z camand is invalid.
    * but if a z is found somewhere else you have garbage infront of a potential
    * command just get rid of it with substr.
    */
    if ($pos === false) {
        return "";
    }
    if ($pos !== 0) {
        $len = strlen($dirty_str);
        $dirty_str = substr($dirty_str, $pos, $len);
    }
    $len = strlen($dirty_str);
    $clean_str = "";
    for ($i = 0; $i < $len; $i++) {
        $chr = $dirty_str[$i];
        if ((ctype_alnum($chr)) | ($chr == "=") |        ($chr == "&") |        ($chr == "[") |        ($chr == "]") |        ($chr == "\\") |        ($chr == "-") |        ($chr == ".")
        ) {
            $clean_str .= $chr;
        }
    } //end for
    return $clean_str;
}

/**
 * This function validates the command value or unary commands with no values.
 *  it simply returns true or false if the command & value are valid.
 */
function bi_validate_commands($cmd_list)
{
    // Parse out the commands for validation.
    $comval = strtok($cmd_list, " &");
    while ($comval !== false) {
        $pos = strpos($comval, "=", 0);
        if ($pos === false) {
            $result = validate_command($comval);
            if (!$result) {
            }
            return $result;
        } else {
            $pos = strpos($comval, "=", 0);
            $command = substr($comval, 0, $pos);
            $result = validate_command($command);
            if (!$result) {
            }
            return $result;
        }
        $comval = strtok("&");
    }//end while
    return true;
}

/**
 * Function validates V4 HTML commands.
 */
function validate_command($command)
{
    if (empty($command)) {
        return false;
    }
    $zoomify_commands = array("zInitialX", "zInitialY", "zInitialZoom", "zMinZoom", "zMaxZoom", "zNavigatorVisible", "zToolbarVisible", "zLogoVisible", "zMinimizeVisible", "zZoomButtonsVisible", "zSliderVisible", "zPanButtonsVisible", "zResetVisible", "zFullViewVisible", "zFullScreenVisible", "zFullPageVisible", "zInitialFullPage", "zProgressVisible", "zTooltipsVisible", "zHelpVisible", "zNavigatorRectangleColor", "zSkinPath", "zHelpPath", "zHelpWidth", "zHelpHeight", "zHelpLeft", "zLogoCustomPath", "zBookmarksSet", "zRulerVisible", "zRulerListType", "zUnits", "zUnitsPerImage", "zPixelsPerUnit", "zSourceMagnification", "zRulerWidth", "zRulerHeight", "zRulerLeft", "zMeasureVisible", "zRotationVisible", "zInitialRotation", "zScreensaver", "zScreensaverSpeed", "zTourPath", "zImageListPath", "zComparisonPath", "zSyncVisible", "zInitialSync", "zOverlayPath", "zSlidePath", "zSlideListTitle",
    "zGalleryVisible",
    "zGalleryWidth",
    "zGalleryHeight",
    "zGalleryLeft",
    "zGalleryTop",
    "zHotspotPath",
    "zHotspotListTitle",
    "zCaptionBoxes",
    "zCaptionTextColor",
    "zCaptionBackColor",
    "zPolygonLineColor",
    "zPolygonFillColor",
    "zCaptionTextVisible",
    "zCaptionBackVisible",
    "zPolygonLineVisible",
    "zPolygonFillVisible",
    "zHotspotsDrawOnlyInView",
    "zCoordinatesVisible",
    "zPreloadVisible",
    "zAnimationPath",
    "zAnimationAxis",
    "zAnimator",
    "zAnimationFlip",
    "zImageSetSliderVisible",
    "zMouseWheel",
    "zGeoCoordinatesPath",
    "zNavigatorWidth",
    "zNavigatorHeight",
    "zNavigatorLeft",
    "zNavigatorTop",
    "zNavigatorFit",
    "zToolbarPosition",
    "zToolbarInternal",
    "zZoomSpeed",
    "zPanSpeed",
    "zPanBuffer",
    "zFadeInSpeed",
    "zConstrainPan",
    "zInteractive",
    "zClickZoom",
    "zDoubleClickZoom",
    "zDoubleClickDelay",
    "zClickPan",
    "zMousePan",
    "zKeys",
    "zMessagesVisible",
    "zAutoResize",
    "zCanvas",
    "zImageProperties",
    "zSmoothPan",
    "zSmoothPanEasing",
    "zSmoothPanGlide",
    "zSmoothZoom",
    "zSmoothZoomEasing",
    "zXMLParametersPath",
    "zTilesPNG",
    "zTileW",
    "zTileH",
    "zOnReady",
    "zBaseZIndex",
    "zDebug",
    "zBrightnessVisible ",
    "zInitialImageFilters",
    "zBrightnessVisible",
    "zContrastVisible",
    "zSharpnessVisible",
    "zBlurrinessVisible",
    "zColorRedVisible",
    "zColorGreenVisible",
    "zColorBlueVisible",
    "zColorRedRangeVisible",
    "zColorGreenRangeVisible",
    "zColorBlueRangeVisible",
    "zGammaVisible",
    "zGammaRedVisible",
    "zGammaGreenVisible",
    "zGammaBlueVisible",
    "zHueVisible",
    "zSaturationVisible",
    "zLightnessVisible",
    "zWhiteBalanceVisible",
    "zNoiseVisible",
    "zGrayscaleVisible",
    "zThresholdVisible",
    "zInversionVisible",
    "zEdgesVisible",
    "zSepiaVisible",
    "zAnnotationPanelVisible",
    "zAnnotationPath",
    "zSimplePath",
    "zAnnotationXMLText",
    "zAnnotationJSONObject",
    "zAnnotationSort",
    "zMarkupMode",
    "zEditMode",
    "zFreehandVisible",
    "zTextVisible",
    "zIconVisible",
    "zRectangleVisible",
    "zPolygonVisible",
    "zSaveHandlerPath",
    "zSaveButtonVisible",
    "zAnnotationsAutoSave",
    "zAnnotationsAddMultiple",
    "zUnsavedEditsTest",
    "zLabelClickSelect",
    "zOnAnnotationReady",
    "zNoPost",
    "zNoPostDefaults",
    "zLabelIconsInternal",
    "zSlidestackPath",
    "zImageSetSliderVisible",
    "zMouseWheel",
    "zUserLogin",
    "zUserNamePrompt",
    "zUserPath",
    "zUserPanelVisible",
    "zTrackingPath",
    "zTrackingEditMode",
    "zTrackingAuto",
    "zTrackingPanelVisible",
    "zInitialTrackingOverlayVisible",
    "zSaveImageHandlerPath",
    "zSaveImageFull",
    "zSaveImageFilename",
    "zSaveImageFormat",
    "zSaveImageCompression",
    "zSaveImageBackColor",
    "zIIIFScheme",
    "zIIIFServer",
    "zIIIFPrefix",
    "zIIIFIdentifier",
    "zIIIFRegion",
    "zIIIFSize",
    "zIIIFRotation",
    "zIIIFQuality",
    "zIIIFFormat",
    "zServerIP",
    "zServerPort",
    "zTileHandlerPath",
    "zImageW",
    "zImageH",
    "zMagnification",
    "zFocal",
    "zQuality",

    );

    $result = false;
    $len = strlen($command);
    if ($len <= 3) {
        return false;
    }
    $pos = strpos($command, "z");
    if ($pos === false | $pos !== 0) {
        return false;
    }
    for ($i = 0; $i <= 190; $i++) {
        if (!(strcmp($command, $zoomify_commands[$i]) === 0)) {
            $result = false;
        } else {
            $result = true;
            break;
        }
    }
    return $result;
}
