<?php

/**
 * @file
 * This file contains all the functions used in the module.
 */

 /**
  * Implements hook_entity_info_alter
  */
function zifimage_entity_info_alter(&$entity_info)
{
    $entity_info['node']['view modes']['new_display_machine_name'] = array(
    'label' => t('New Display Name'),
    'custom settings' => '',
    );
}


/**
 * Define the zifimage files basepath.
 */

function _zifimage_basepath()
{
    $basepath = variable_get('file_public_path', conf_path() . '/files') . '/zifimage/Images';
    if (!file_prepare_directory($basepath, FILE_CREATE_DIRECTORY)) {
        watchdog('zifimage', 'Could not create path %path.', array('%path' => $basepath), WATCHDOG_ERROR);
    }
    return $basepath;
}

/**
 * Retrieve the node specific zifimage path.
 */

function _zifimage_nodepath($node)
{
    $path = _zifimage_basepath();
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
        return $path;
    }
    return false;
}

/**
 *  Retrieve the file specific zifimage path.
 */

function _zifimage_filepath($node, $fid)
{
    $path = _zifimage_nodepath($node);
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
        return $path;
    }
    return false;
}

/**
 * Implements hook_file_mimetype_mapping_alter().
 */

function zifimage_file_mimetype_mapping_alter(&$mapping)
{
    // Add support for zif.
    $new_mappings['zif'] = 'image/zif';

    foreach ($new_mappings as $extension => $mime_type) {
        if (!in_array($mime_type, $mapping['mimetypes'])) {
            // If the mime type does not already exist, add it.
            $mapping['mimetypes'][] = $mime_type;
        }

        // Get the index of the mime type and assign the extension to that key.
        $index = array_search($mime_type, $mapping['mimetypes']);
        $mapping['extensions'][$extension] = $index;
    }
}

/**
 *  This function implements a work around for select form object.
 *  I could not figure out how to get select to stop returning the index
 *  of the #options array when I want the selected string. This crude hack
 * regenerates the array and uses the index to return the desired string.
 */

function zifimage_selected_skin()
{
    $zifimage_skins_list = array();
    $library_path = libraries_get_path('zoomify', false) . '/Assets/Skins';

    $zifimage_skins_list = scandir($library_path);

    array_shift($zifimage_skins_list);
    array_shift($zifimage_skins_list);
    $n = variable_get('zifimage_viewer_skins', 0);
    return $zifimage_skins_list[$n];
}

/**
 * This function compines the local HTML commands and the Configured HTML
 * commands. The local commands take precidince ovr Configured commands
 * but a configured command cannot be removed only over ridden with the
 * same command using a different value. Note that in this context the
 * HTML Command does not refer to actual HTML code but to the commands
 *  passed to the JS viewer through HTML processes.  There are 260 some odd
 * commands defined in V4. I have not updated the commands to match V5.
 * Consider the entire security features as experimental at this time.
 */

function combine_commands($local, $site)
{
    if (empty($local)) {
        return $site;
    }
    if (empty($site)) {
        return $local;
    }
    $comval = strtok($site, "&");
    while ($comval !== false) {
        $pos = strpos($comval, "=", 0);

        if ($pos === false) {
            if (empty($local)) {
                $local .= $comval;
            } else {
                $local .= "&" . $comval;
            }
        } else {
            // Has a value here.
            $pos = strpos($comval, "=", 0);
            $command = substr($comval, 0, $pos);
            $pos = strpos($local, $command);
            if ($pos === false) {
                if (empty($local)) {
                    $local .= $comval;
                } else {
                    $local .= '&' . $comval;
                }
            }
        }
        $comval = strtok("&");
    }//end while
    return $local;
}

/**
 *  Impl;ements the XML file command inserrtion function
 */

function get_xml($instring, $node, $nid, $fid)
{
    // Get the xml type.
    $xml_type = 0;
    if (isset($node->{'zifimage_op_mode'})) {
        $xml_type = $node->{'zifimage_op_mode'}['und']['0']['value'];
    }
    // Get the file name & URL
    // try to use /sites/default/files/zifimzge/Assets/xml_files/filemane.
    if (isset($node->{'zifimage_txt_xml'}['und'])) {
        $xml_file_name = $node->{'zifimage_txt_xml'}['und']['0']['filename'];
        $xml_file_name = str_replace('txt', 'xml', $xml_file_name);
        $xml_uri = file_build_uri(('zifimage/Assets/txt_xml_files/' . "$nid/$fid/$xml_file_name"));
        $file_url = file_create_url($xml_uri);
    } else {
        exit;
    }
    $file_url = str_replace('.txt', '.xml', $file_url);
    switch ($xml_type) {
        case $xml_type == ZIFIMAGE_XML_TYPE_DESTINATIONS:
            if (empty($instring)) {
                $instring .= 'zTourPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zTourPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_ANIMATIONS:
            if (empty($instring)) {
                $instring .= 'zAnimationPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zAnimationPath=' . $file_url;
                return $instring;
            }

            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_IMAGE_LIST:
            if (empty($instring)) {
                $instring .= 'zImageListPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zImageListPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_COMPARISON:
            if (empty($instring)) {
                $instring .= 'zComparisonPath=' . $file_url;
                if (ZIFIMAGE_DEBUG !== 0) {
                    echo "Switch Compatison:$instring<br>";
                }
                return $instring;
            } else {
                $instring .= '&zComparisonPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_OVERLAY:
            if (empty($instring)) {
                $instring .= 'zOverlayPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zOverlayPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_SLIDE:
            if (empty($instring)) {
                $instring .= 'zSlidePath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zSlidePath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_HOTSPOT:
            if (empty($instring)) {
                $instring .= 'zHotspotPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zHotSpotPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_ANNOTATION:
            if (empty($instring)) {
                $instring .= 'zAnnotationPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zAnnotationPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == zifimage_XMLTYP_SINGLE_IMAGE:
            $instring = '';
            break;
    }//end switch
}

/**
 * Define Fip Archive Unpact Action.
 */
function zifimage_action_info()
{
    return array(
    'zifimage_basic_action' => array(
      'label' => t('zifimage Basic Action'),
      'type' => 'system',
      'configurable' => false,
      'triggers' => array('any'),
    ),
    );
}

/**
 * Defines the Action Flag for file maintainance.
 */

function zifimage_basic_action(&$entity, $context)
{
    if (isset($context['node']->{'type'})) {
        if ($context['node']->{'type'} !== 'zifimage') {
            variable_set('zifimage_update_files', false);
            return;
        } else {
            variable_set('zifimage_update_files', true);
        }
    }
}

/**
 * Delete unused image subdirecrories in node.
 */

function node_image_dir_prune($nid)
{
    // Get a array of image directories associated with the node.
    $pattern = './sites/default/files/zifimage/Images/' . $nid . '/*';

    $image_dir_array = glob($pattern, GLOB_ONLYDIR);

    if (empty($image_dir_array)) {
        return;
    } else {
        foreach ($image_dir_array as $path) {
            $pattern = $path . '/*.*';
            $files_ar = glob($pattern);
            if (empty($files_ar)) {
                if (file_unmanaged_delete_recursive($path) === false) {
                }
            }
        }//end foreach image_dir_array  as $path
    }//endelse
}

/**
 * Deletes unused Subdirectories with eh the node.
 */

function node_prune_xml($nid)
{

    $pattern = './sites/default/files/zifimzge/' . $nid . '/*';
    $xml_dir_array = glob($pattern, GLOB_ONLYDIR);
    if (empty($xml_dir_array)) {
        return;
    }
    foreach ($xml_dir_array as $xml_dir) {
        $pattern = $xml_dir . '/^.txt';
        $file_array = glob($pattern);
        if (empty($file_array)) {
            if (file_unmanaged_delete_recursive($xml_dir) === false) {
                if (ZIFIMAGE_DEBUG !== 0) {
                    drupal_set_message(" File delete Failed on: $xml_dir check file permissions", 'warning');
                }
            }
        }// End if
    }// End foreach xml dir array
}

/**
 * Unpacks ZIP Archives.
 */

function node_image_unpacker($nid)
{
    $pattern = './sites/default/files/zifimage/Images/' . $nid . '/*/*.*';
    $zip_path_array = glob($pattern);
    if (empty($zip_path_array)) {
        return;
    }
    foreach ($zip_path_array as $path) {
        $len = strlen($path);
        $ext = substr($path, -3, 3);
        if (($ext === 'zif') || ($ext == 'jpg') || ($ext === 'png') || ($ext === 'gif')) {
            return;
        }//endif
        $path2 = substr($path, 0, $len - 4);
        if ((file_exists($path2)) && (is_dir($path2))) {
            return;
        } else {
            $archiver = archiver_get_archiver($path);
            if ($archiver) {
                $archiver->extract($path2);
            }//endif
        }//end for each
    }
}

/**
 * function scans txt file and writes out XML file.
 */

function node_fix_xml($node)
{

    if (empty($node->{'zifimage_master_txt_xml'})) {
        return;
    }//Endif enpty
    // ==========================build filename>fileURI array.
    $file2url = array();
    $filename = '';
    // Image files.
    $i = 0;
    while (($node->{'zifimage_bulk_images'} !== array()) && (isset($node->{'zifimage_bulk_images'}['und'][$i]))) {
        $filename = $node->{'zifimage_bulk_images'}['und'][$i]['filename'];
        $file_uri = $node->{'zifimage_bulk_images'}['und'][$i]['uri'];
        $file_url = file_create_url($file_uri);
        // The file uri may contane other file types besided .zip.
        $file_url = str_replace('.zip', '', $file_url);
        $file2url[$filename] = $file_url;
        $i++;
    }// End while zifimage_images is set
    // hotspots.
    $i = 0;
    while (!empty($node->{'zifimage_hotspot_icons'}['und'][$i])) {
        $filename = $node->{'zifimage_hotspot_icons'}['und'][$i]['filename'];
        $filename = rtrim($filename);
        $filename = trim($filename);
        $file_uri = $node->{'zifimage_hotspot_icons'}['und'][$i]['uri'];
        $file_url = file_create_url($file_uri);
        $file2url[$filename] = $file_url;
        $i++;
    }//End while hotspot images
    // audio media.
    $i = 0;
    while (!empty($node->{'zifimage_audio_media'}['und'][$i])) {
        $filename = $node->{'zifimage_audio_media'}['und'][$i]['filename'];
        $file_uri = $node->{'zifimage_audio_media'}['und'][$i]['uri'];
        $file_url = file_create_url($file_uri);
        $file2url[$filename] = $file_url;
        $i++;
    }//End while hotspot images
    // xml files.
    $i = 0;
    while (!empty($node->{'zifimage_txt_xml'}['und'][$i])) {
        $filename = $node->{'zifimage_txt_xml'}['und'][$i]['filename'];
        $file_uri = $node->{'zifimage_txt_xml'}['und'][$i]['uri'];
        $file_url = file_create_url($file_uri);
        $file_url = str_replace('.txt', '.xml', $file_url);
        $file2url[$filename] = $file_url;
        $i++;
    }// End while zifimage_txt_xml
    // the file reference array is built.
    // =========================================================.
    $i = 0;
    $txt_list = array();
    if (!empty($node->{'zifimage_master_txt_xml'}['und'][0]['uri'])) {
        $txt_list[] = $node->{'zifimage_master_txt_xml'}['und'][0]['uri'];
    }
    if (!empty($node->{'zifimage_genparam_txt_xml'}['und'][0]['uri'])) {
        $txt_list[] = $node->{'zifimage_genparam_txt_xml'}['und'][0]['uri'];
    }
    $i = 0;
    while (isset($node->{'zifimage_bulk_txt_xml'}['und'][$i]['uri'])) {
        $txt_list[] = $node->{'zifimage_bulk_txt_xml'}['und'][$i]['uri'];
        $i++;
    }//End while
    // all xmlfiles in list now proces them.
    foreach ($txt_list as $file_uri) {
        $out_file_uri = str_replace('.txt', '.xml', $file_uri);
        $fp = fopen($file_uri, 'rb');
        if ($fp) {
        } else {
            exit;
        }
        while ($current_line = fgets($fp)) {
            $in_txt[] = $current_line;
        }
        fclose($fp);
        // File contena in array.
        if (!empty($in_txt)) {
            foreach ($in_txt as $line) {
                // ==================================================================.
                fixline($line, $file2url);
                // ===============================================================.
                $out_xml[] = $line;
            }//end foreach line
            if (!empty($out_xml)) {
                if (!($fp = fopen($out_file_uri, 'wb'))) {
                    break;
                }
                $out_str = implode($out_xml);
                // Write string to our opened file.
                if ((fwrite($fp, $out_str) === false)) {
                    break;
                }
                fclose($fp);
            } else {
                break;
            }
        }// end if
    }//end for each
}

/**
 * Function replaces filename with file URL.
 *
 * This function is called once for each line in the txt file. When a filename
 * containing line is encountered the filename is replaced with the correct
 * path. The calling routine passes an array containing the filename => paths
 * by reference. The matching is case sensitive. Drupal must not transliteraqte
 * the file names or the match will fail. Use only ASCII charicters and the
 * undercore in filenames. The use of 3 letter extensions is assumed throuout
 * the code. Do not use extensions like .html. THe module can recognise only
 * the following file extensions: '.txt', '.xml', '.zip', '.zif', '.jpg',
 * '.png', '.gif'.
 */

function fixline(&$line, $file2path)
{
    // if(function_exists('dvm')&&ZIFIMAGE_DEBUG>0)dvm($file2path);.
    switch ($line) {
        case contains_word($line, 'MEDIA') !== false:
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("MEDIA detected in $line", 'status');
            }
            $filename = str_replace('MEDIA=', '', $line);
            $filename = str_replace('"', '', $filename);
            $filename = rtrim($filename);
            $filename = trim($filename);
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Filename resolved to: $filename", 'status');
            }
            $file_url = get_value($filename, $file2path);
            if ($file_url !== false) {
                $line = '            MEDIA="' . "$file_url" . '"' . "\n";
            } else {
                $file_url = "$filename Not Found check spelling and case";
                $line = '            MEDIA="' . "$file_url" . '"' . "\n";
            }
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("line set to:$line", 'status');
            }
            break;

        case strpos($line, 'AUDIO=') !== false:
            $fn = str_replace('AUDIO="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      AUDIO="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Founf. Check Spelling and case";
                $line = '      AUDIO="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'ANNOTATIONS=') !== false:
            $fn = str_replace('ANNOTATIONS="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      ANNOTATIONS="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found check spelling and case";
                $line = '      ANNOTATIONS="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'TRACKING=') !== false:
            $fn = str_replace('TRACKING=', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      TRACKING="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found cect spelling and case";
                $line = '      TRACKING="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'HOTASPOTPATH=') !== false:
            $fn = str_replace('HOTSPOTPATH="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      HOTSPOTPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn not found check spelling adn case";
                $line = '      HOTSPOTPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'IMAGELIST=') !== false:
            $fn = str_replace('IMAGELIST="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_valuye($fn, $file2path);
            if (file_url !== false) {
                $line = '      IMAGELIST="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found check spellinhg and case";
                $line = '      IMAGELIST="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'HOTSPOT=') !== false:
            $fn = str_replace('HOTSPOT="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      HOTSPOT="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found check spelling adn case";
                $line = '      HOTSPOT="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'IMAGEPATH=') !== false:
            $fn = str_replace('IMAGEPATH=', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if (file_url !== false) {
                $line = '      IMAGEPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn NOT FOUND check speling and case";
                $line = '      IMAGEPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'HOTSPOTXMLPATH=') !== false:
            $fn = str_replace('IMAAGEPATH="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      IMAGEPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = $fn . ' filenot found check spelling and case';
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'ANNOTATIONXMLPATH=') !== false:
            $fn = str_replace('ANNOTATIONXMLPATH="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found Check spelling and case";
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'SLIDEXMLPATH=') !== false:
            $fn = str_replace('SLIDEXMLPATH=', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = getvalue($fn, $file2path);
            if ($file_url !== false) {
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = " $fn Not Found Check Spelling and Case ";
                $line     = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            }

            break;
    }
}

/**
 * Functon returns the value of an associative array given the key or FALSE.
 */
function get_value(&$my_key, &$my_array)
{

    if (array_key_exists($my_key, $my_array)) {
        return $my_array[$my_key];
    } else {
        return false;
    }
}

/**
 * Function searches a srting for an occurance of a WORD.
 */
function contains_word($str, $word)
{
    return !!preg_match('#\\b' . preg_quote($word, '#') . '\\b#i', $str);
}

/**
 * function returns list of availiable viewers.
 */
function get_viewer_list()
{
    $viewer_list = array();
    $pos = strlen('./sites/all/libraries/zoomify/');
    $i = 1;
    foreach (glob("./sites/all/libraries/zoomify/*.js") as $filename) {
        $len = strlen($filename);
        $viewer_list[$i] = substr($filename, $pos, $len);
        $i++;
    }
    return $viewer_list;
}

/**
 * Functon Returns the selecte cviewer.
 */
function zifimage_selected_viewer(&$node)
{
    $viewer_list = get_viewer_list();
    if (!empty($node->{'zifimage_viewer'})) {
        $n = $node->{'zifimage_viewer'}['und'][0]['value'];
    } else {
        $n = variable_get('zifimage_conf_selected_viewer', 1);
    }
    $selected = $viewer_list[$n];
    return $selected;
}
