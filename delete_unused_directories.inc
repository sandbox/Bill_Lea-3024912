<?php

/**
 * @file
 * File maintenance routines.
 */

/**
 * This function deletes directories left when their nodes have been deleted.
 * It does this by compiling a list of the node ID's currently active in the
 * database. If this list is empty then all the nodes that are found by GLOB
 * are invalid and must be deleted. if the node id list is populated then we
 * have of build lists of the disk node directories for
 * Images, Assets/Hotspots, Assets/Media, and Assets/text_xml_files. In each
 * catagory we need to individually abort testing if glob does not find a node
 * directory. Otherwise we build a list of nid => $Path. The paths have to
 * uri's. If a path NIS is not in the nid_list ffron the database we delete
 * the path. The difficulty in this code is actually deternining the NodeID.
 * It appears that Strrpos does not act as expected and return the position of
 * the charticter found searching backwards from the iend.
 */
function delete_unused_directories()
{
    clearstatcache();
    if (ZIFIMAGE_DEBUG !== 0) {
        echo "Entering function delete_unused_directories<br>";
    }
    // Build $Active_node_list.
    $active_node_list = array();
    $node_type = 'zifimage';
    $active_node_list = db_query("SELECT nid FROM {node} WHERE type = :type", array(':type' => $node_type))->fetchCol();
    if (empty($active_node_list)) {
        // ==========================================================================
        // Clean zifimage/Images.
        $pattern = './sites/default/files/zifimage/Images/*';
        $dir_list = glob($pattern, GLOB_ONLYDIR);
        if (!empty($dir_list) || $dir_list !== false) {
            foreach ($dir_list as $path) {
                $path = file_build_uri($path);
                file_unmanaged_delete_recursive($path);
            }//end foreach
        }//end if
        echo "leaving zifimage/Image No acxtive Node Cleaning<br.";
        // ===========================================================================.
        $patern = './sites/default/files/zifimage/Assets/Hotspots/*';
        $dir_list = glob($patern, GLOB_ONLYDIR);
        if (!empty($dir_list) || $dir_list !== false) {
            foreach ($dir_list as $path) {
                $path = file_build_uri($path);
                file_unmanaged_delete_recursive($path);
            }//end foreach
        }
        // Endif.
        // ==========================================================================.
        $patern = './sites/default/files/zifimage/Assets/Media/*';
        $dir_list = glob($patern, GLOB_ONLYDIR);
        if (!empty($dir_list) || $dir_list !== false) {
            foreach ($dir_list as $path) {
                $path = file_build_uri($path);
                file_unmanaged_delete_recursive($path);
            }//End foreach
        }//endif
        // ===========================================================================.
        $patern = './sites/default/files/zifimage/Assets/txt_xml_files/*';
        $dir_list = glob($patern, GLOB_ONLYDIR);
        if (!empty($dir_list) || $dir_list !== false) {
            foreach ($dir_list as $path) {
                $path = file_build_uri($path);
                file_unmanaged_delete_recursive($path);
            }//end foreach
        }//end if
        return;
    }//endif
    // ================================= Activwe nodes found =====================
    //
    // Active nodes found do comparisons
    // zifimage/Images.
    $patern = './sites/default/files/zifimage/Images/*';
    $dir_list = glob($patern, GLOB_ONLYDIR);
    if (!empty($dir_list) || $dir_list !== false) {
        if (!delete_Inactive_driectory($active_node_list, $dir_list)) {
            drupal_set_message(t("Directory delete Failed"), 'error');
        }
    }//endif
    // -------------------------------------------------------------------------------
    // zifimage/Assets/Hotsopts.
    echo "Cleaning ziffimage/Assets/Hotspots<br>";
    $patern = './sites/default/files/zifimage/Assets/Hotspots/*';
    $dir_list = glob($patern, GLOB_ONLYDIR);
    if (!empty($dir_list) || $dir_list !== false) {
        if (!delete_Inactive_driectory($active_node_list, $dir_list)) {
            drupal_set_message(f("Directory delete Failed"), 'error');
        }
    }//End if
    // ---------------------------------------------------------------------------
    // zifimage/Assets/Media.
    $patern = './sites/default/files/zifimage/Assets/Media/*';
    $dir_list = glob($patern, GLOB_ONLYDIR);
    if (!empty($dir_list) || $dir_list !== false) {
        if (!delete_Inactive_driectory($active_node_list, $dir_list)) {
            drupal_set_message(t("Directory delete Failed"), 'error');
        };
    }//End if
    // ---------------------------------------------------------------------------
    // zifimage/Assets/txt_xml_files.
    $patern = './sites/default/files/zifimage/Assets/txt_xml_files/*';
    $dir_list = glob($patern, GLOB_ONLYDIR);
    if (!empty($dir_list) || $dir_list !== false) {
        if (!delete_Inactive_driectory($active_node_list, $dir_list)) {
            drupal_set_message(t("Directory delete Failed"), 'error');
        }
    }//end if
}

/**
 * Function to return the NID from the end of a path or URL string.
 * if none is found returns FALSE.
 **/
function nid_from_url($url)
{
    $pos = strrpos($url, '/');
    if ($pos === false) {
        return false;
    } else {
        $id_str = substr($url, $pos + 1);
        if (!is_numeric($id_str)) {
        } else {
            return $id_str;
        }//end else
    }//end else
}

/**
 * This function deletes the directories not found in the active node list.
 */
function delete_inactive_driectory(&$active_node_list, $dir_list)
{
    if (!empty($dir_list) || $dir_list !== false) {
        foreach ($dir_list as $dir_path) {
            $node_id = nid_from_url($dir_path);
            if ($node_id !== false && is_numeric($node_id)) {
                $dir_path = file_build_uri($dir_path);
                if (!in_array($node_id, $active_node_list)) {
                    delete_files_dir($dir_path);
                }//end if
            }//endif
        }//end foreach
    }
    // End if.
    else {
        return false;
    }
    return true;
}

/**
 * Delete managed files in a folder and their DB records.
 */
function delete_files_in_folder($uri)
{
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'file')->propertyCondition('uri', $uri . '/%', 'LIKE');
    $result = $query->execute();

    if (isset($result['file'])) {
        $fids = array_keys($result['file']);

        foreach ($fids as $fid) {
            $file = file_load($fid);
            file_delete($file);
        }
    }
}

function delete_files_dir($uri)
{
    delete_files_in_folder($uri);
    file_unmanaged_delete_recursive($uri);
}
