<?php

/**
 * Function scans txt file lines and replaces filenames with full file URLs.
 */
function fixline(&$line, $file2path)
{
    switch ($line) {
        case containsWord($line, 'MEDIA') !== false:
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("MEDIA detected in $line", 'status');
            }
            $filename = str_replace('MEDIA=', '', $line);
            $filename = str_replace('"', '', $filename);
            $filename = rtrim($filename);
            $filename = trim($filename);
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Filename resolved to: $filename", 'status');
            }
            $file_url = get_value($filename, $file2path);
            if ($file_url !== false) {
                $line = '            MEDIA="' . "$file_url" . '"' . "\n";
            } else {
                $file_url = "$filename Not Found check spelling and case";
                $line = '            MEDIA="' . "$file_url" . '"' . "\n";
            }
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("line set to:$line", 'status');
            }
            break;

        case strpos($line, 'AUDIO=') !== false:
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message('The substring AUDIO exists in given string.', 'status');
            }
            $fn = str_replace('AUDIO="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      AUDIO="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Founf. Check Spelling and case";
                $line = '      AUDIO="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'ANNOTATIONS=') !== false:
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message('The substring ANNOTATIONS exists in given string.', 'status');
            }
            $fn = str_replace('ANNOTATIONS="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      ANNOTATIONS="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found check spelling and case";
                $line = '      ANNOTATIONS="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'TRACKING=') !== false:
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message('The substring TRACKING exists in given string.', 'status');
            }
            $fn = str_replace('TRACKING=', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      TRACKING="' . $file_uri . '"' . "\n";
            } else {
                $file_uri = "$fn Not Found cect spelling and case";
                $line = '      TRACKING="' . $file_uri . '"' . "\n";
            }
            break;

        case strpos($line, 'HOTASPOTPATH=') !== false:
            $fn = str_replace('HOTSPOTPATH="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      HOTSPOTPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn not found check spelling adn case";
                $line = '      HOTSPOTPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'IMAGELIST=') !== false:
            $fn = str_replace('IMAGELIST="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_valuye($fn, $file2path);
            if (file_url !== false) {
                $line = '      IMAGELIST="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found check spellinhg and case";
                $line = '      IMAGELIST="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'HOTSPOT=') !== false:
            $fn = str_replace('HOTSPOT="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      HOTSPOT="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found check spelling adn case";
                $line = '      HOTSPOT="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'IMAGEPATH=') !== false:
            $fn = str_replace('IMAGEPATH=', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if (file_url !== false) {
                $line = '      IMAGEPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn NOT FOUND check speling and case";
                $line = '      IMAGEPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'HOTSPOTXMLPATH=') !== false:
            $fn = str_replace('IMAAGEPATH="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      IMAGEPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = $fn . ' filenot found check spelling and case';
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'ANNOTATIONXMLPATH=') !== false:
            $fn = str_replace('ANNOTATIONXMLPATH="', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = get_value($fn, $file2path);
            if ($file_url !== false) {
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = "$fn Not Found Check spelling and case";
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            }
            break;

        case strpos($line, 'SLIDEXMLPATH=') !== false:
            $fn = str_replace('SLIDEXMLPATH=', '', $line);
            $fn = str_replace('"', '', $fn);
            $fn = rtrim($fn);
            $fn = trim($fn);
            $file_url = getvalue($fn, $file2path);
            if ($file_url !== false) {
                $line = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            } else {
                $file_url = " $fn Not Found Check Spelling and Case ";
                $line     = '      ANNOTATIONXMLPATH="' . $file_url . '"' . "\n";
            }
            break;
    }
}
/**
 * Function returns the value of an associative aray or FALSE.
 */
function get_value(&$my_key, &$my_array)
{
    if (ZIFIMAGE_DEBUG !== 0) {
        echo "getvalue: Key=$my_key, <br>Valuie=$my_array[$my_key]<br>";
    }
    if (array_key_exists($my_key, $my_array)) {
        return $my_array[$my_key];
    } else {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message(" Array key $my_key not found in array check key spelling and case", 'error');
        }
        return false;
    }
}

/**
 * Function detects whole words.
 */
function contains_word($str, $word)
{
    return !!preg_match('#\\b' . preg_quote($word, '#') . '\\b#i', $str);
}

/**
 * function returns a list of availiable viewers.
 */
function get_viewer_list()
{
    $viewer_list = array();
    $pos = strlen('./sites/all/libraries/zoomify/');
    $i = 1;
    foreach (glob("./sites/all/libraries/zoomify/*.js") as $filename) {
        $len = strlen($filename);
        $viewer_list[$i] = substr($filename, $pos, $len);
        $i++;
    }
    if (function_exists('dvm') && ZIFIMAGE_DEBUG > 0) {
        dvm($viewer_list);
    }
    return $viewer_list;
}

/**
 * Function returns the selected viewer.
 */
function zifimage_selected_viewer(&$node)
{
    $viewer_list = get_viewer_list();
    if (!empty($node->{'zifimage_viewer'})) {
        $n = $node->{'zifimage_viewer'}['LANGUAGE_NONE'][0]['value'];
    } else {
        $n = variable_get('zifimage_conf_selected_viewer', 1);
        if (ZIFIMAGE_DEBUG !== 0) {
            echo "Config viewer returned Viewer  index n as $n<br/>";
        }
    }
    $selected = $viewer_list[$n];
    if (ZIFIMAGE_DEBUG !== 0) {
        drupal_set_message("Final viewer $n selected was: $selected", 'status');
    }
    return $selected;
}

/**
 * Helper function to define the zifimage files basepath.
 */
function _zifimage_basepath()
{
    $basepath = variable_get('file_public_path', conf_path() . '/files') . '/zifimage/Images';
    if (!file_prepare_directory($basepath, FILE_CREATE_DIRECTORY)) {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message(t('Could not create path %path.', array('%path' => $basepath)), 'error');
        }
        watchdog('zifimage', 'Could not create path %path.', array('%path' => $basepath), WATCHDOG_ERROR);
    }
    return $basepath;
}

/**
 * Helper function to retrieve the node specific zifimage path.
 */
function _zifimage_nodepath($node)
{
    $path = _zifimage_basepath();
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
        return $path;
    }
    return false;
}

/**
 * Helper function to retrieve the file specific zifimage path.
 */
function _zifimage_filepath($node, $fid)
{
    $path = _zifimage_nodepath($node);
    if (file_prepare_directory($path, FILE_CREATE_DIRECTORY)) {
        return $path;
    }
    return false;
}

/**
 * Implements hook_file_mimetype_mapping_alter().
 */
function zifimage_file_mimetype_mapping_alter(&$mapping)
{
    // Add support for zif.
    $new_mappings['zif'] = 'image/zif';
    foreach ($new_mappings as $extension => $mime_type) {
        if (!in_array($mime_type, $mapping['mimetypes'])) {
            // If the mime type does not already exist, add it.
            $mapping['mimetypes'][] = $mime_type;
        }

        // Get the index of the mime type and assign the extension to that key.
        $index = array_search($mime_type, $mapping['mimetypes']);
        $mapping['extensions'][$extension] = $index;
    }
}

/**
 * This function implements a work around for select form object.
 *  I could not figure out how to get select to stop returning the index
 *  of the #options array when I want the selected string. This crude hack
 * regenerates the array and uses the index to return the desired string.
 */
function zifimage_selected_skin()
{
    $zifimage_skins_list = array();
    $library_path = libraries_get_path('zoomify', false) . '/Assets/Skins';

    $zifimage_skins_list = scandir($library_path);

    array_shift($zifimage_skins_list);
    array_shift($zifimage_skins_list);
    $n = variable_get('zifimage_viewer_skins', 0);
    return $zifimage_skins_list[$n];
}
/**
 * This function implements a work around for select form object.
 *  I could not figure out how to get select to stop returning the index
 *  of the #options array when I want the selected string. This crude hack
 * regenerates the array and uses the index to return the desired string.
 */
function combine_commands($local, $site)
{
    if (empty($local)) {
        return $site;
    }
    if (empty($site)) {
        return $local;
    }
    $comval = strtok($site, "&");
    while ($comval !== false) {
        $pos = strpos($comval, "=", 0);
        if ($pos === false) {
            if (empty($local)) {
                $local .= $comval;
            } else {
                $local .= "&" . $comval;
            }
        } else {
            // Has a value here.
            $pos = strpos($comval, "=", 0);
            $command = substr($comval, 0, $pos);
            $pos = strpos($local, $command);
            if ($pos === false) {
                if (empty($local)) {
                    $local .= $comval;
                } else {
                    $local .= '&' . $comval;
                }
            }
        }
        $comval = strtok("&");
    }//end while
    return $local;
}


/**
 * Impl;ements the XML file command inserrtion function.
 */
function get_xml($instring, $node, $nid, $fid)
{
    // Get the xml type.
    $xml_type = 0;
    if (ZIFIMAGE_DEBUG !== 0) {
        drupal_set_message("Looking for XML file at node:$nid, File:$fid", 'status');
    }
    if (isset($node->{'zifimage_op_mode'})) {
        $xml_type = $node->{'zifimage_op_mode'}['LANGUAGE_NONE']['0']['value'];
    }
    if (ZIFIMAGE_DEBUG !== 0) {
        echo "Op Mode was:$xml_type<br />";
    }
    // Get the file name & URL
    // try to use /sites/default/files/zifimzge/Assets/xml_files/filemane.
    if (isset($node->{'zifimage_txt_xml'}['LANGUAGE_NONE'])) {
        $xml_file_name = $node->{'zifimage_txt_xml'}['LANGUAGE_NONE']['0']['filename'];
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("xml Filename first was $xml_file_name ", 'status');
        }
        $xml_file_name = str_replace('txt', 'xml', $xml_file_name);
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("xml filname reset to:$xml_file_name ", 'status');
        }
        $xml_uri = file_build_uri(('zifimage/Assets/txt_xml_files/' . "$nid/$fid/$xml_file_name"));
        $file_url = file_create_url($xml_uri);
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("url= $file_url " . 'status');
        }
    } else {
        if (ZIFIMAGE_DEBUG !== 0) {
            echo "No xml defined<br>";
        }
        exit;
    }
    $file_url = str_replace('.txt', '.xml', $file_url);
    switch ($xml_type) {
        case $xml_type == ZIFIMAGE_XML_TYPE_DESTINATIONS:
            if (empty($instring)) {
                $instring .= 'zTourPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zTourPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_ANIMATIONS:
            if (empty($instring)) {
                $instring .= 'zAnimationPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zAnimationPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_IMAGE_LIST:
            if (empty($instring)) {
                $instring .= 'zImageListPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zImageListPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_COMPARISON:
            if (empty($instring)) {
                $instring .= 'zComparisonPath=' . $file_url;
                if (ZIFIMAGE_DEBUG !== 0) {
                    echo "Switch Compatison:$instring<br>";
                }
                return $instring;
            } else {
                $instring .= '&zComparisonPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_OVERLAY:
            if (empty($instring)) {
                $instring .= 'zOverlayPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zOverlayPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_SLIDE:
            if (empty($instring)) {
                $instring .= 'zSlidePath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zSlidePath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == ZIFIMAGE_XML_TYPE_HOTSPOT:
            if (empty($instring)) {
                $instring .= 'zHotspotPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zHotSpotPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == zifimage_XML_TYPE_ANNOTATION:
            if (empty($instring)) {
                $instring .= 'zAnnotationPath=' . $file_url;
                return $instring;
            } else {
                $instring .= '&zAnnotationPath=' . $file_url;
                return $instring;
            }
            break;

        case $xml_type == zifimage_XMLTYP_SINGLE_IMAGE:
            $instring = '';
            break;

        default:
            break;
    }//end switch
}

actions_synchronize(actions_list(), true);

/**
 * Define Fip Archive Unpact Action.
 */
function zifimage_action_info()
{
    return array(
    'zifimage_basic_action' => array(
      'label' => t('zifimage Basic Action'),
      'type' => 'system',
      'configurable' => false,
      'triggers' => array('any'),
    ),
    );
}

/**
 * Function defines action to set flag for file/dir update.
 */
function zifimage_basic_action(&$entity, $context)
{
    if (ZIFIMAGE_DEBUG !== 0) {
        drupal_set_message(t('Node save/update detected: action fired'));
    }
    if (isset($context['node']->{'type'})) {
        if ($context['node']->{'type'} !== 'zifimage') {
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Zip Archive Not found Returning", 'status');
            }
            variable_set('zifimage_update_files', false);
            return;
        } else {
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("zifimage node found Setting Update Files TRUE", 'status');
            }
            variable_set('zifimage_update_files', true);
        }
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message(t('zifimage action fired'));
        }
    }
}
function node_image_dir_prune($nid)
{
    // Get a array of image directories associated with the node.
    $pattern = './sites/default/files/zifimage/Images/' . $nid . '/*';
    $image_dir_array = glob($pattern, GLOB_ONLYDIR);
    if (empty($image_dir_array)) {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("$image_dir_array was empty return" . 'stAtus');
        }
        return;
    } else {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("File Array populated", 'status');
        }
        foreach ($image_dir_array as $path) {
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Testing Path:$path", 'status');
            }
            $pattern = $path . '/*.*';
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("new pattern was: $pattern", 'status');
            }
            $files_ar = glob($pattern);
            if (empty($files_ar)) {
                if (ZIFIMAGE_DEBUG !== 0) {
                    drupal_set_message("No files found delete $path", 'status');
                }
                if (file_unmanaged_delete_recursive($path) === false) {
                    if (ZIFIMAGE_DEBUG !== 0) {
                        drupal_set_message("File Delete Failed on $path Check file permissions", 'warning');
                    }
                }
            }
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Files found RETURNING", 'status');
            }
        }//end foreach image_dir_array  as $path
    }//end else
}

/**
 * Function pruns directories fron nide when no no longerf needed.
 */
function node_prune_xml($n)
{
    $pattern = './sites/default/files/zifimzge/' . $n . '/*';
    $xml_dir_array = glob($pattern, GLOB_ONLYDIR);
    if (empty($xml_dir_array)) {
        return;
    }
    foreach ($xml_dir_array as $xml_dir) {
        $pattern = $xml_dir . '/*.txt';
        $file_array = glob($pattern);
        if (empty($file_array)) {
            if (file_unmanaged_delete_recursive($xml_dir) === false) {
                if (ZIFIMAGE_DEBUG !== 0) {
                    drupal_set_message(" File delete Failed on: $xml_dir check file permissions", 'warning');
                }
            }
        }//endif
    }
}
/**
 * Function unpacks zip archives.
 */
function node_image_unpacker($nid)
{
    if (ZIFIMAGE_DEBUG !== 0) {
        drupal_set_message("node_image_unpacker entered node:$nid", 'status');
    }
    $pattern = './sites/default/files/zifimage/Images/' . $nid . '/*/*.*';
    $zip_path_array = glob($pattern);
    if (empty($zip_path_array)) {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("zip files not found RETURNING", 'status');
        }
        return;
    }
    if (function_exists('dvm') && ZIFIMAGE_DEBUG > 0) {
        dvm($zip_path_array);
    }
    foreach ($zip_path_array as $path) {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("Glob $pattern Returned $path", 'status');
        }
        $len = strlen($path);
        $ext = substr($path, -3, 3);
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message(" Path Search ext returned as:$ext", 'status');
        }
        if (($ext === 'zif') || ($ext == 'jpg') || ($ext === 'png') || ($ext === 'gif')) {
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Image file $ext found Returning", 'status');
            }
            if (ZIFIMAGE_DEBUG !== 0) {
                echo "Unpacker found image file $ext returning<br>";
            }
            return;
        }//endif
        $path2 = substr($path, 0, $len - 4);
        if (ZIFIMAGE_DEBUG !== 0) {
            echo "Path2 was:$path2<br>";
        }
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("Path2 nwas: $path2 ", 'status');
        }
        if ((file_exists($path2)) && (is_dir($path2))) {
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Tile Structure found RETURNING", 'status');
            }
            if (ZIFIMAGE_DEBUG !== 0) {
                echo "$path2 was a directory returning<br>";
            }
            return;
        } else {
            if (ZIFIMAGE_DEBUG !== 0) {
                drupal_set_message("Found zip file which has not been unpacked UNPACK it", 'status');
            }
            $archiver = archiver_get_archiver($path);
            if ($archiver) {
                $archiver->extract($path2);
            }//endif
        }//end for each
    }
    if (ZIFIMAGE_DEBUG !== 0) {
        drupal_set_message("node_image_unpacker returns", 'status');
    }
}

/**
 * Function rewrites txt control files into XML files.
 */
function node_fix_xml($node)
{
    if (ZIFIMAGE_DEBUG !== 0) {
        echo "Node Fix XML entered<br>";
    }
    if (empty($node->{'zifimage_master_txt_xml'})) {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("No XML files exist Returning", 'status');
        }
        return;
    }//End if
    // ==========================build filename=>fileURI array.
    if (ZIFIMAGE_DEBUG !== 0) {
        echo "Build File Translation array<br>";
    }
    $file2uri = array();
    $filename = '';
    if (ZIFIMAGE_DEBUG !== 0) {
        drupal_set_message("building list of image files", 'status');
    }
    // Image files.
    $i = 0;
    while (($node->{'zifimage_bulk_images'} !== array()) && (isset($node->{'zifimage_bulk_images'}['LANGUAGE_NONE'][$i]))) {
        if (ZIFIMAGE_DEBUG !== 0) {
            echo "While Loop 1 I:$i,]<br>";
        }
        if (ZIFIMAGE_DEBUG !== 0) {
            echo "While Loop 1 I:$i,<br>";
        }
        $filename = $node->{'zifimage_bulk_images'}['LANGUAGE_NONE'][$i]['filename'];
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("Build File Array -> Filename was: $filename", 'status');
        }
        $file_uri = $node->{'zifimage_bulk_images'}['LANGUAGE_NONE'][$i]['uri'];
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("File URI was: $file_uri", 'status');
        }
        $file_url = file_create_url($file_uri);
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("File URL was: $file_url", 'status');
        }
        // The file uri may contane other file types besided .zip.
        $file_url = str_replace('.zip', '', $file_url);
        $file2url[$filename] = $file_url;
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("filename2url[$filename]= $file2url[$filename]", 'status');
        }
        if (ZIFIMAGE_DEBUG !== 0) {
            echo "filename2url[$filename]= $file2url[$filename]<br>";
        }
        $i++;
    }// End while zifimage_images is set
    // hotspots.
    $i = 0;
    while (!empty($node->{'zifimage_hotspot_icons'}['LANGUAGE_NONE'][$i])) {
        $filename = $node->{'zifimage_hotspot_icons'}['LANGUAGE_NONE'][$i]['filename'];
        $filename = rtrim($filename);
        $filename = trim($filename);
        $file_uri = $node->{'zifimage_hotspot_icons'}['LANGUAGE_NONE'][$i]['uri'];
        $file_url = file_create_url($file_uri);
        $file2url[$filename] = $file_url;
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("filenameurl_array[$filename]= $file2url[$filename]", 'status');
        }
        $i++;
    }//End while hotspot images
    // audio media.
    $i = 0;
    while (!empty($node->{'zifimage_audio_media'}['LANGUAGE_NONE'][$i])) {
        $filename = $node->{'zifimage_audio_media'}['LANGUAGE_NONE'][$i]['filename'];
        $file_uri = $node->{'zifimage_audio_media'}['LANGUAGE_NONE'][$i]['uri'];
        $file_url = file_create_url($file_uri);
        $file2url[$filename] = $file_url;
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("filenameuri_array[$filename]= $file2url[$filename]", 'status');
        }
        $i++;
    }//End while hotspot images
    // xml files.
    $i = 0;
    while (!empty($node->{'zifimage_txt_xml'}['LANGUAGE_NONE'][$i])) {
        $filename = $node->{'zifimage_txt_xml'}['LANGUAGE_NONE'][$i]['filename'];
        $file_uri = $node->{'zifimage_txt_xml'}['LANGUAGE_NONE'][$i]['uri'];
        $file_url = file_create_url($file_uri);
        $file_url = str_replace('.txt', '.xml', $file_url);
        $file2url[$filename] = $file_url;
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("filenameuri_array[$filename]= $file2uri[$filename]", 'status');
        }
        $i++;
    }// End while zifimage_txt_xml
    // the file reference array is built.
    // =========================================================.
    $i = 0;
    $txt_list = array();
    if (!empty($node->{'zifimage_master_txt_xml'}['LANGUAGE_NONE'][0]['uri'])) {
        $txt_list[] = $node->{'zifimage_master_txt_xml'}['LANGUAGE_NONE'][0]['uri'];
    }
    if (!empty($node->{'zifimage_genparam_txt_xml'}['LANGUAGE_NONE'][0]['uri'])) {
        $txt_list[] = $node->{'zifimage_genparam_txt_xml'}['LANGUAGE_NONE'][0]['uri'];
    }
    $i = 0;
    while (isset($node->{'zifimage_bulk_txt_xml'}['LANGUAGE_NONE'][$i]['uri'])) {
        $txt_list[] = $node->{'zifimage_bulk_txt_xml'}['LANGUAGE_NONE'][$i]['uri'];
        $i++;
    }//End while
    // all xmlfiles in list now proces them.
    foreach ($txt_list as $file_uri) {
        $out_file_uri = str_replace('.txt', '.xml', $file_uri);
    }
    if (ZIFIMAGE_DEBUG !== 0) {
        drupal_set_message("Output File path set to:$out_file_uri", 'status');
    }
    $fp = fopen($file_uri, 'rb');
    if ($fp) {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("input file:$file_uri opened sucessfuly", 'status');
        }
    } else {
        if (ZIFIMAGE_DEBUG !== 0) {
            drupal_set_message("Input file $file_uri Failed to open", 'status');
        }
        exit;
    }
    while ($current_line = fgets($fp)) {
        $in_txt[] = $current_line;
    }
    fclose($fp);
    // File contena in array.
    if (!empty($in_txt)) {
        foreach ($in_txt as $line) {
            // ==================================================================.
            fixline($line, $file2url);
            // ===============================================================.
            $out_xml[] = $line;
        }//end foreach line
        if (!empty($out_xml)) {
            if (($fp = fopen($out_file_uri, 'wb')) === false) {
                drupal_set_message(
                    t(
                        "Cannot open output file @filename at @out_file_uri",
                        array('@filename' => $filename , '@out_file_uri' => $out_file_uri)
                    ),
                    'error'
                );
            }
            $out_str = implode($out_xml);
            // Write string to our opened file.
            if ((fwrite($fp, $out_str) === false)) {
                 drupal_set_message("Cannot write to file $out_file_uri", 'error');
                 fclose($fp);
                 return;
            }
            fclose($fp);
        } else {
            drupal_set_message(t("The xml output data was missisng."), 'error');
            return;
        }
    }
}
