<?php

/**
 * @file
 * This set of functions tries o operate on the content add form.
 * It needs validation  for more items.
 */

function zifimage_node_validate($form_id, $form_values)
{

    if (empty($form_values)) {
        return;
    }

    if ($form_id->{'type'} !== 'zifimage') {
        return;
    }
    $test = $form_id->{'zifimage_local_var'}['und']['0']['value'];
    if (!empty($test)) {
        $security_level = variable_get('zifimage_security', 3);
        $result = validate_user_commands($test, $security_level);
        if ($result) {
        } else {
            $message = 'Validation failed revise commnds entered';
            $name = 'zifimage_local_var';
            form_set_error($name, $message, null);
        }
    }
}
