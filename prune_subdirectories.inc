<?php

/**
 * @file
 * @file
 * this function erases unused directories and files in a given node.
 * First build a list of directories to be examined. There are several cases.
 * files/zifimage/Assets/Hotspots
 * files/zifimage/Assets/Media
 * files/zifimage/Assets/txt_to_xml_files
 * files/zifimage/Images
 * These will have subdirectories such as files/zifimage/Images/nid/fid
 * in the c ase of the Images subdirectory a valid directory will contail a
 * file and possibly a directory structure. allowable extensipons are:
 * zip zif jpg png.
 * for files/zifimage/Assets/txt_to_xml_files/$nid/$fid
 * the subdirectory must contain a txt file to be valid
 * The subdirectory iles/zifimage/Assets/Media/$nid/$fid must contain a mp3 file
 * The subdirectory files/zifimage/Assets/Hotspots/$nid/$fid may contain either
 * jpg or png files.
 */

function prune_subdrirectories($nid)
{

    // Hotspots.
    $pattern = 'files/zifimage/Assets/Hotspots/$nid/*';
    $dir_list = glob($pattern, GLOB_ONLYDIR);
    foreach ($dir_list as $dir) {
        $file_list = glob($dir);
        $erase_subdirectory = true;
        foreach ($file_list as $file) {
            if (!is_dir($file)) {
                // Must be a file. We can test or assume it's corect.
                $erase_subdirectory = false;
            }//endif
        }//end foreach
        if ($erase_subdirectory) {
            $path = 'public://' . $dir;
            file_delete_recursivve($path);
        }// Endif
    }// End foreach
    // Media.
    $pattern = 'files/zifimage/Assets/Media/$nid/*';
    $dir_list = glob($pattern, GLOB_ONLYDIR);
    foreach ($dir_list as $dir) {
        $file_list = glob($dir);
        $erase_subdirectory = true;
        foreach ($file_list as $file) {
            if (!is_dir($file)) {
                // Must be a file. We can test or assume it's corect.
                $erase_subdirectory = false;
            }// Endif
        }// End foreach
        if ($erase_subdirectory) {
            $path = 'public://' . $dir;
            file_delete_recursivve($path);
        }//End if
    }//End foreach
    // Txt_to_xml.
    $pattern = 'files/zifimage/Assets/txt_to_xml/$nid/*';
    $dir_list = glob($pattern, GLOB_ONLYDIR);
    foreach ($dir_list as $dir) {
        $file_list = glob($dir);
        $erase_subdirectory = true;
        foreach ($file_list as $file) {
            if (!is_dir($file)) {
                // Must be a file. We can test or assume it's corect.
                if (strpos($file, '.txt') !== false) {
                    $erase_subdirectory = false;
                } else {
                    $erase_subdirectory = true;
                }
            }// End foreach
            if ($erase_subdirectory) {
                $path = 'public://' . $dir;
                file_delete_recursivve($path);
            }// Endif
        }// End foreach
    }// End foreach
    // Images.
    $pattern = 'files/zifimage/Images/$nid/*';
    $dir_list = glob($pattern, GLOB_ONLYDIR);
    foreach ($dir_list as $dir) {
        $file_list = glob($dir);
        $erase_subdirectory = true;
        foreach ($file_list as $file) {
            if (!is_dir($file)) {
                // Must be a file. We can test or assume it's corect.
                if (strpos($file, '.txt') !== false) {
                    $erase_subdirectory = false;
                }
            }//end foreach
            if ($erase_subdirectory) {
                $path = 'public://' . $dir;
                file_delete_recursivve($path);
            }// Endif
        }// End foreach
    }
}
