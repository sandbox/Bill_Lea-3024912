<?php

/**
 * @file
 * This file containes code to support the slide mode of operation.
 *
 * The slide mode of operation requires a special directory
 * structure. When operating with slides copies of the slide files must be
 * located in the main image subdirectory. These functions provide for this
 * function and synchronization. They are called by the preprocess function
 * on aaving or updating the node content.
 */

/**
 * Update files for slides
 */
function update_slide_files(&$node)
{
    move_slide_files($node);
    purge_slide_liles($node);
}

/**
 * This function moves the files from the bulk directories to the main Image
 * file directory.  It is used when the operating mode is SLIDE only. The
 * function does an unmagaged file copy. So file size may be a factor of
 * account is limited in Disk capacity.
 */
function move_slide_files(&$node)
{
    $mode = $node->{'zifimage_op_mode'}['und'][0]['value'];
    if ($mode !== ZIFIMAGE_XML_TYPE_SLIDE) {
        // No need to move anything.
        return;
    }
    $destination = $node->{'zifimage_zif'}['und'][0]['uri'];
    $filename = $node->{'zifimage_zif'}['und'][0]['filename'];
    $destination = str_replace($filename, '', $destination);
    $i = 0;
    while (isset($node->{'zifimage_bulk_images'}['und'][$i])) {
        $source = $node->{'zifimage_bulk_images'}['und'][$i]['uri'];
        file_unmanaged_copy($source, $destination, FILE_EXISTS_REPLACE);
        $i++;
    }//end while
}

/**
 * This function removes unused unmanaged files from the zifimage_zif directory.
 */
function purge_slide_liles(&$node)
{
    /*
    * build an array of all the files in zifimage_zif directory -- needs to be
    * in form of filename => file_path.
    */
    $pattern = $node->{'zifimage_zif'}['und'][0]['uri'];
    $pattern = str_replace($node->{'zifimage_zif'}['und'][0]['filename'], '', $pattern);
    $pattern .= '*.*';
    $file_list = glob($pattern);
    $file_content = array();
    foreach ($file_list as $file_path) {
        $fn = strrchr($file_path, '/');
        $fn = str_replace('/', '', $fn);
        $file_content[$fn] = $file_path;
    }//end foreach
    /*
    * Build list of Required files
    */
    $i = 0;
    $fn = '';
    $file_path = '';
    $required_files = array();
    while (isset($node->{'zifimage_bulk_images'}['und'][$i])) {
        $file_path = $node->{'zifimage_bulk_images'}['und'][$i]['uri'];
        $fn = $node->{'zifimage_bulk_images'}['und'][$i]['filename'];
        $required_files[$fn] = $file_path;
        $i++;
    }//end while
    /*
    * check for unneeded files
    */
    foreach ($file_content as $filename => $path) {
        if (!(array_key_exists($filename, $required_files))) {
            file_unmanaged_delete($path);
            unset($file_content[$filename]);
        }//endif
    }//end foreach
    /*
    * Copy needed files
    *
    */
    $destination = $node->{'zifimage_zif'}['und'][0]['uri'];
    $fn = $node->{'zifimage_zif'}['und'][0]['filename'];
    $destination = str_replace($fn, '', $destination);
    foreach ($required_files as $filename => $path) {
        if (!(array_key_exists($filename, $file_content))) {
            file_unmanaged_copy($path, $destination, FILE_EXSISTS_REPLACE);
        }//endif
    }//end foreach
}
