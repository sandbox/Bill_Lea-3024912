<?php

/**
 * @file
 * The security of the routines in the zifimage module.
 */

function validate_user_commands($dirty_str, $security_level)
{
    // Empty strings are always valid.
    if (empty($dirty_str)) {
        return true;
    } else {
        switch ($security_level) {
            case zifimage_SECURITY_LEVEL_NONE:
                return true;

            break;

            case zifimage_SECURITY_LEVEL_CLEAN_STRINGS:
                $res = validate_str($dirty_str);
                return $res;

            break;

            case zifimage_SECURITY_LEVEL_CLEAN_COMMANDS:
                print_r($dirty_str);
                $valid_str = validate_str($dirty_str);

                if ($valid_str) {
                    if (validate_command_str($dirty_str)) {
                        return true;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                break;
        }//end switch
    }//end if
}

/**
 * Implements the srting cleaning routine. This routine searches the string
 * for illegal characters. It reports the locatkion in the string of and
 * finds.
 */
function validate_str($dirty_str)
{
    // If there is no zcommand return the empty string.
    $pos = strpos($dirty_str, "z");
    /*
    * all valid command string should start with a z no z camand is invalid.
    */
    if ($pos === false) {
        return false;
    }
    if ($pos !== 0) {
        drupal_set_message(t('Failed z test. First Character must be a lower case z for a valid entry'), 'error');
        return false;
    }

    $len = strlen($dirty_str);
    for ($i = 0; $i < $len; $i++) {
        $chr = $dirty_str[$i];
        if ((ctype_alnum($chr)) | ($chr == "=") | ($chr == "&") | ($chr == "[") | ($chr == "]") | ($chr == "\\") | ($chr == "-") | ($chr == ".")
        ) {
            $result = true;
        } else {
            $result = false;
            break;
        }
    } //end for
    return $result;
}

/**
 * his routinew validates command strings and their expected value types.
 * Each command is matched against a list of valid commads generated from
 * Zoomify documentation and* the commands expected value type;
 * the set of value types are:
 * INT   - integer
 * FLOAT - floaqting point
 * BOOL  -  a 0 or 1
 * PERC  - 0 to 100
 * URI   - A file path URI.
 * STR   _ an alphaneumeric string.
 *
 * These variable types will be checket with regex expressions. Taken from
 * the web. Hopefully they will work.
 */

/**
 * this function takes the complete string of command-values and parses it into
 * single command value pairs which it sends to a helper funbction to validate
 * further. The command-value pair is split and the com mand is matched. if a
 * match is fount the type pf the value is matched against the expected type
 * associated with the command.
 */
function validate_command_str($cmd_list)
{
    // Parse out the commands for validation.
    $comval = strtok($cmd_list, "        &");
    while ($comval !== false) {
        $pos = strpos($comval, "=", 0);
        if ($pos === false) {
            $value = null;
            $result = validate_command($comval, $value);
            if (!$result) {
            }
            return $result;
        } else {
            $pos = strpos($comval, "=", 0);
            $len = strlen($comval);
            $command = substr($comval, 0, $pos);
            $value = substr($comval, $pos + 1, $len);
            $result = validate_command($command, $value);
            if (!$result) {
            }
            return $result;
        }
        $comval = strtok("&");
    }//end while
    return true;
}

/**
 * This function validates the command-value str
 */

function validate_command($command, $value)
{
    if (empty($command)) {
        return false;
    }
    $zoomify_commands = array(
    "zInitialX"                       => "int",
    "zInitialY"                       => "int",
    "zInitialZoom"                    => "int",
    "zMinZoom"                        => "Num0_100",
    "zMaxZoom"                        => "Num0_100",
    "zNavigatorVisible"               => "bool",
    "zToolbarVisible"                 => "bool",
    "zLogoVisible"                    => "bool",
    "zMinimizeVisible"                => "bool",
    "zZoomButtonsVisible"             => "bool",
    "zSliderVisible"                  => "bool",
    "zPanButtonsVisible"              => "bool",
    "zResetVisible"                   => "bool",
    "zFullViewVisible"                => "bool",
    "zFullScreenVisible"              => "bool",
    "zFullPageVisible"                => "bool",
    "zInitialFullPage"                => "bool",
    "zProgressVisible"                => "bool",
    "zTooltipsVisible"                => "bool",
    "zHelpVisible"                    => "bool",
    "zNavigatorRectangleColor"        => "",
    "zSkinPath"                       => "",
    "zHelpPath"                       => "",
    "zHelpWidth"                      => "num ",
    "zHelpHeight"                     => "num ",
    "zHelpLeft"                       => "bool",
    "zLogoCustomPath"                 => "path",
    "zBookmarksSet"                   => "",
    "zRulerVisible"                   => "bool",
    "zRulerListType"                  => "",
    "zUnits"                          => "unit",
    "zUnitsPerImage"                  => "num ",
    "zPixelsPerUnit"                  => "num ",
    "zSourceMagnification"            => "num ",
    "zRulerWidth"                     => "num ",
    "zRulerHeight"                    => "num ",
    "zRulerLeft"                      => "color",
    "zMeasureVisible"                 => "bool",
    "zRotationVisible"                => "bool",
    "zInitialRotation"                => "bool",
    "zScreensaver"                    => "bool",
    "zScreensaverSpeed"               => "num ",
    "zTourPath"                       => "path",
    "zImageListPath"                  => "path",
    "zComparisonPath"                 => "path",
    "zSyncVisible"                    => "bool",
    "zInitialSync"                    => "",
    "zOverlayPath"                    => "path",
    "zSlidePath"                      => "path",
    "zSlideListTitle"                 => "string_alf",
    "zGalleryVisible"                 => "bool",
    "zGalleryWidth"                   => "num ",
    "zGalleryHeight"                  => "num ",
    "zGalleryLeft"                    => "num ",
    "zGalleryTop"                     => "",
    "zHotspotPath"                    => "",
    "zHotspotListTitle"               => "string_an",
    "zCaptionBoxes"                   => "",
    "zCaptionTextColor"               => "",
    "zCaptionBackColor"               => "",
    "zPolygonLineColor"               => "",
    "zPolygonFillColor"               => "",
    "zCaptionTextVisible"             => "bool",
    "zCaptionBackVisible"             => "bool",
    "zPolygonLineVisible"             => "bool",
    "zPolygonFillVisible"             => "bool",
    "zHotspotsDrawOnlyInView"         => "bool",
    "zCoordinatesVisible"             => "bool",
    "zPreloadVisible"                 => "bool",
    "zAnimationPath"                  => "path",
    "zAnimationAxis"                  => "Xl-file",
    "zAnimator"                       => "",
    "zAnimationFlip"                  => "",
    "zImageSetSliderVisible"          => "bool",
    "zMouseWheel"                     => "bool",
    "zGeoCoordinatesPath"             => "path",
    "zNavigatorWidth"                 => "num ",
    "zNavigatorHeight"                => "num ",
    "zNavigatorLeft"                  => "bool",
    "zNavigatorTop"                   => "bool",
    "zNavigatorFit"                   => "",
    "zToolbarPosition"                => "",
    "zToolbarInternal"                => "bool",
    "zZoomSpeed"                      => "num ",
    "zPanSpeed"                       => "num ",
    "zPanBuffer"                      => "",
    "zFadeInSpeed"                    => "num ",
    "zConstrainPan"                   => "",
    "zInteractive"                    => "bool",
    "zClickZoom"                      => "bool",
    "zDoubleClickZoom"                => "bool",
    "zDoubleClickDelay"               => "num",
    "zClickPan"                       => "",
    "zMousePan"                       => "",
    "zKeys"                           => "",
    "zMessagesVisible"                => "bool",
    "zAutoResize"                     => "bool",
    "zCanvas"                         => "",
    "zImageProperties"                => "",
    "zSmoothPan"                      => "path",
    "zSmoothPanEasing"                => "",
    "zSmoothPanGlide"                 => "bool",
    "zSmoothZoom"                     => "bool",
    "zSmoothZoomEasing"               => "",
    "zXMLParametersPath"              => "path",
    "zTilesPNG"                       => "bool",
    "zTileW"                          => "int",
    "zTileH"                          => "int",
    "zOnReady"                        => "",
    "zBaseZIndex"                     => "int",
    "zDebug"                          => "int",
    "zImageFiltersVisible"            => "bool",
    "zBrightnessVisible "             => "bool",
    "zInitialImageFilters"            => "bool",
    "zBrightnessVisible"              => "bool",
    "zContrastVisible"                => "bool",
    "zSharpnessVisible"               => "bool",
    "zBlurrinessVisible"              => "bool",
    "zColorRedVisible"                => "bool",
    "zColorGreenVisible"              => "bool",
    "zColorBlueVisible"               => "bool",
    "zColorRedRangeVisible"           => "bool",
    "zColorGreenRangeVisible"         => "bool",
    "zColorBlueRangeVisible"          => "bool",
    "zGammaVisible"                  => "bool",
    "zGammaRedVisible"               => "bool",
    "zGammaGreenVisible"             => "bool",
    "zGammaBlueVisible"              => "bool",
    "zHueVisible"                    => "bool",
    "zSaturationVisible"             => "bool",
    "zLightnessVisible"              => "bool",
    "zWhiteBalanceVisible"           => "bool",
    "zNoiseVisible"                  => "bool",
    "zGrayscaleVisible"              => "bool",
    "zThresholdVisible"              => "bool",
    "zInversionVisible"              => "bool",
    "zEdgesVisible"                  => "bool",
    "zSepiaVisible"                  => "bool",
    "zAnnotationPanelVisible"        => "bool",
    "zAnnotationPath"                => "path",
    "zSimplePath"                    => "path",
    "zAnnotationXMLText"             => "",
    "zAnnotationJSONObject"          => "bool",
    "zAnnotationSort"                => "bool",
    "zMarkupMode"                    => "bool",
    "zEditMode"                      => "bool",
    "zFreehandVisible"               => "bool",
    "zTextVisible"                   => "bool",
    "zIconVisible"                   => "bool",
    "zRectangleVisible"              => "bool",
    "zPolygonVisible"                => "bool",
    "zSaveHandlerPath"               => "",
    "zSaveButtonVisible"             => "bool",
    "zAnnotationsAutoSave"           => "",
    "zAnnotationsAddMultiple"        => "",
    "zUnsavedEditsTest"              => "",
    "zLabelClickSelect"              => "",
    "zOnAnnotationReady"             => "",
    "zNoPost"                        => "",
    "zNoPostDefaults"                => "",
    "zLabelIconsInternal"            => "",
    "zSlidestackPath"                => "path",
    "zImageSetSliderVisible"         => "bool",
    "zMouseWheel"                    => "bool",
    "zUserLogin"                     => "bool",
    "zUserNamePrompt"                => "String-alpha",
    "zUserPath"                      => "path",
    "zUserPanelVisible"              => "bool",
    "zTrackingPath"                  => "path",
    "zTrackingEditMode"              => "",
    "zTrackingAuto"                  => "",
    "zTrackingPanelVisible"          => "bool",
    "zInitialTrackingOverlayVisible" => "bool",
    "zSaveImageHandlerPath"          => "path",
    "zSaveImageFull"                 => "bool",
    "zSaveImageFilename"             => "fname",
    "zSaveImageFormat"               => "imgfmt",
    "zSaveImageCompression"          => "comp",
    "zSaveImageBackColor"            => "color",
    "zIIIFScheme"                    => "",
    "zIIIFServer"                    => "",
    "zIIIFPrefix"                    => "",
    "zIIIFIdentifier"                => "String-alpha",
    "zIIIFRegion"                    => "String-alpha",
    "zIIIFSize"                      => "num",
    "zIIIFRotation"                  => "bool",
    "zIIIFQuality"                   => "num",
    "zIIIFFormat"                    => "imgfmt",
    "zServerIP"                      => "IP",
    "zServerPort"                    => "num",
    "zTileHandlerPath"               => "path",
    "zImageW"                        => "W",
    "zImageH"                        => "f ",
    "zMagnification"                 => "num",
    "zFocal"                         => "num",
    "zQuality"                       => "num",
    // Giant associative array defined.
    );

    $result = false;
    $len = strlen($command);
    if ($len <= 3) {
        drupal_set_message(t('Command too short <3 characters - invalid command'), 'warning');
        return false;
    }
    $pos = strpos($command, "z");
    if ($pos === false | $pos !== 0) {
        drupal_set_message(t('First letter of command MUST be a lower case z'), 'warning');
        return false;
    }
    foreach ($zoomify_commands as $cmd => $value_type) {
        if ((strcmp($command, $cmd)) === 0) {
            // Match found -check value against type.
            $result = true;
            // $result=value_validator($value, $value_type);.
            $result = verify_value_type($value, $value_type);
            break;
        } else {
            $result = false;
        }
    }
    if ($result === false) {
        drupal_set_message(t('Invalid or malformed comand/value type  encountered'), 'error');
    }
    return $result;
}

/**
 * This function validates values by type. Its a giant switch.
 * it taqkes two inputs the value and the expected type and returns true or
 * false.
 */
function value_validator($value, $value_type)
{
    if (empty($value)) {
        return true;
    }
    switch ($value_type) {
        case 'num':
            $result = preg_match('/^\d+$/', $value);
            break;

        case 'int':
            $result = preg_match('/^\d+$/', $value);
            break;

        case 'float':
            $result = preg_match('/^[-+]?[0-9]+[.]?[0-9]*([eE][-+]?[0-9]+)?$/', $value);
            break;

        case 'percent':
            // Intiger test.
            $result = preg_match('/^\d+$/', $value);
            if ($result === 1) {
                if (($value >= 0) & ($value <= 100)) {
                    $result = true;
                }
            } else {
                $result = false;
            }
            break;

        case 'path':
            if (preg_match('/^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w ]*))+\.(txt|TXT)$/', $value)) {
                $result = true;
            } else {
                $result = false;
            }
            break;

        case 'filename':
            if (preg_match('/^[\w0-9&#228;&#196;&#246;&#214;&#252;&#220;&#223;\-_]+\.[a-zA-Z0-9]{2,6}$/', $value)) {
                $result = true;
            } else {
                $result = false;
            }
            break;

        case 'name':
            $result = true;
            break;

        case 'bool':
            if ((strcmp($value, '0') === 0) | (strcmp($value, '1') === 0)
            ) {
                $result = true;
            } else {
                $result = false;
            }
            break;

        case 'color':
            if (preg_patch('/^#?([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/', $value)) {
                $result = true;
            } else {
                $result = false;
            }
            break;

        default:
            $result = true;
    }//end switch
    return $result;
}
