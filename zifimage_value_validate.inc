<?php

/**
 * @file
 * This file contains routines to validate HTML command value types.
 */

/**
 * This function validates values by type. Its a giant switch.
 * it taqkes two inputs the value and the expected type and returns
 * true or false.
 */
function verify_value_type($value, $value_type)
{
    switch ($value_type) {
        case (strcmp("int", $value_type) === 0):
            $result = preg_match("/^\d+$/", $value);
            break;

        case (strcmp("num", $value_type) === 0):
            $result = preg_match("/^[-+]?[0-9]+[.]?[0-9]*([eE][-+]?[0-9]+)?$/", $value);
            break;

        case (strcmp("percent", $value_type) === 0):
            // Intiger test.
            $result = preg_match("/^\d+$/", $value);
            if ($result === 1) {
                if (($value >= 0) & ($value <= 100)) {
                    $result = true;
                }
            } else {
                $result = false;
            }

            break;

        case (strcmp("path", $value_type) === 0):
            if (preg_match("/^(([a-zA-Z]:)|(\\{2}\w+)\$?)(\\(\w[\w ]*))+\.(txt|TXT)$/", $value)) {
                $result = true;
            } else {
                $result = false;
            }

            break;

        case (strcmp("filename", $value_type) === 0):
            if (preg_match("/^[\w0-9&#228;&#196;&#246;&#214;&#252;&#220;&#223;\-_]+\.[a-zA-Z0-9]{2,6}$/", $value)) {
                $result = true;
            } else {
                $result = false;
            }

            break;

        case (strcmp("name", $value_type) === 0):
            $result = true;

            break;

        case (strcmp("bool", $value_type) === 0):
            if ((strcmp("0", $value) === 0) | (strcmp($value, "1", 0) === 0)) {
                $result = true;
            } else {
                $result = false;
            }

            break;

        case (strcmp("color", $value_type) === 0):
            if (preg_patch("/^#?([a-f]|[A-F]|[0-9]){3}(([a-f]|[A-F]|[0-9]){3})?$/", $value)) {
                $result = true;
            } else {
                $result = false;
            }

            break;

        default:
            $result = true;
    }//end switch
    return $result;
}
